package at.spenger.junit.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.time.LocalDateTime;

public class Club {
	private List<Person> l;

	public Club() {
		l = new ArrayList<>();
	}

	public boolean enter(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.add(p);
	}

	public int numberOf() {
		return l.size();
	}

	public List<Person> getPersons() {
		return Collections.unmodifiableList(l);
	}

	public int average() {
		if (numberOf() == 0)
			return 0;
		int total = 0, year = LocalDateTime.now().getYear(), day =  LocalDateTime.now().getDayOfYear();
		for (Person p : l) {
			total += year - p.getBirthday().getYear();
			if (day >= p.getBirthday().getDayOfYear())
				total--;
		}
		
		return total / numberOf();
	}
	public void sortAge(){
		Collections.sort(l,new CompAge());
	}
	public void sortName(){
		Collections.sort(l,new CompName());
	}
	public void austreten(Person p){
		l.remove(p);
	}
	public Person youngest(){
		Person p2=null;
		
		if (numberOf() == 0)
			return null;
		int total = 0, year = LocalDateTime.now().getYear(), day =  LocalDateTime.now().getDayOfYear(),yp=0;
		for (Person p : l) {
			total += year - p.getBirthday().getYear();
			if (day >= p.getBirthday().getDayOfYear())
				total--;
			if(p2==null){
				p2=p;
				yp=total;
			}else if(total<yp){
				p2=p;
			}
		}
		return p2;
				
	}
	
}
