package at.spenger.junit.domain;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class ClubTest {
	
	@Test
	public void test() {
		Club c=new Club();
		Person p1=new Person("Da", "Bures", LocalDate.parse("1945-05-07"), Person.Sex.MALE),p2=new Person("Haft", "Befehl", LocalDate.parse("2000-09-13"), Person.Sex.MALE),p3=new Person("Da", "Hatzinger", LocalDate.parse("1950-10-27"), Person.Sex.MALE);
		c.enter(p1);
		c.enter(p2);
		c.enter(p3);
		c.sortAge();
		assertEquals(p2, c.getPersons().get(0));
	}
	@Test
	public void testaverage(){
		Club c=new Club();
		Person p1=new Person("Schircha", "H�tsinger", LocalDate.parse("1945-05-07"), Person.Sex.MALE),p2=new Person("Dreh", "Riegler", LocalDate.parse("2000-09-13"), Person.Sex.MALE),p3=new Person("Totalschaden", "H�tsinger", LocalDate.parse("1950-10-27"), Person.Sex.MALE);
		c.enter(p1);
		c.enter(p2);
		c.enter(p3);
		assertEquals(50, c.average());
	}
	@Test
	public void testname(){
		Club c=new Club();
		Person p1=new Person("Schircha", "H�tsinger", LocalDate.parse("1945-05-07"), Person.Sex.MALE),p2=new Person("Dreh", "Riegler", LocalDate.parse("2000-09-13"), Person.Sex.MALE),p3=new Person("Totalschaden", "H�tsinger", LocalDate.parse("1950-10-27"), Person.Sex.MALE);
		c.enter(p1);
		c.enter(p2);
		c.enter(p3);
		assertEquals(p1, c.getPersons().get(0));
	}
	@Before
	public void setUp() throws Exception {
	
	}
}
